using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    [SerializeField] AudioClip coinPickUpSFX;

    private int coinPoints = 1;
    private bool coinExists = false;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision is CapsuleCollider2D)
        {
            if (!coinExists)
            {
                coinExists = true;
                FindObjectOfType<GameSession>().AddScore(coinPoints);
                AudioSource.PlayClipAtPoint(coinPickUpSFX, Camera.main.transform.position, 0.1f);
                Destroy(gameObject);
            }
        }
    }
}
