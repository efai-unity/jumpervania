using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VerticalScroll : MonoBehaviour
{
    [Tooltip("Game units per seconds")]
    [SerializeField] float scrollSpeed = 0.1f;

    private void Update()
    {
        transform.Translate(new Vector2(0f, scrollSpeed * Time.deltaTime));
    }
}
