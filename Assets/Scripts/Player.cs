using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class Player : MonoBehaviour
{
    [SerializeField] AudioClip deathSFX;
    [SerializeField] AudioClip jumpSFX;

    float runSpeed = 6f;
    float jumpSpeed = 28f;
    float climbSpeed = 5f;
    Vector2 deathKick = new Vector2(5f, 15f);

    bool isAlive = true;
    float gravityScaleAtStart;

    Rigidbody2D rb;
    Animator animator;
    CapsuleCollider2D bodyCollider;
    BoxCollider2D feetCollider;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        bodyCollider = GetComponent<CapsuleCollider2D>();
        feetCollider = GetComponent<BoxCollider2D>();
        gravityScaleAtStart = rb.gravityScale;
    }

    private void Update()
    {
        if (!isAlive)
        {
            return;
        }

        Run();
        ClimbLadder();
        Jump();
        FlipSprite();
        Death();
    }

    private bool IsPlayerMoving()
    {
        return Mathf.Abs(rb.velocity.x) > Mathf.Epsilon;
    }

    private void Run()
    {
        float controlThrow = CrossPlatformInputManager.GetAxis("Horizontal"); // value is between -1 to 1
        Vector2 playerVelocity = new Vector2(controlThrow * runSpeed, rb.velocity.y);
        rb.velocity = playerVelocity;

        animator.SetBool("Running", IsPlayerMoving());
    }

    private void ClimbLadder()
    {
        if (!feetCollider.IsTouchingLayers(LayerMask.GetMask("Climbing")))
        {
            animator.SetBool("Climbing", false);
            rb.gravityScale = gravityScaleAtStart;
            return;
        }

        float controlThrow = CrossPlatformInputManager.GetAxis("Vertical");
        Vector2 climbVelocity = new Vector2(rb.velocity.x, controlThrow * climbSpeed);
        rb.velocity = climbVelocity;
        rb.gravityScale = 0f;

        bool playerIsClimbing = Mathf.Abs(rb.velocity.y) > Mathf.Epsilon;

        animator.SetBool("Climbing", playerIsClimbing);
    }

    private void Jump()
    {
        if (!feetCollider.IsTouchingLayers(LayerMask.GetMask("Ground", "Climbing")))
        {
            return;
        }

        if (CrossPlatformInputManager.GetButtonDown("Jump"))
        {
            Vector2 verticalVelocity = new Vector2(0f, jumpSpeed);
            rb.AddForce(verticalVelocity, ForceMode2D.Impulse);
            AudioSource.PlayClipAtPoint(jumpSFX, Camera.main.transform.position, 0.2f);
        }
    }

    private void Death()
    {
        if (bodyCollider.IsTouchingLayers(LayerMask.GetMask("Enemy", "Hazards")))
        {
            isAlive = false;
            GetComponent<Rigidbody2D>().velocity = deathKick;
            animator.SetBool("Death", true);
            AudioSource.PlayClipAtPoint(deathSFX, Camera.main.transform.position, 0.5f);

            StartCoroutine(DelayDeath());
        }
    }

    private IEnumerator DelayDeath()
    {
        yield return new WaitForSeconds(1f);

        FindObjectOfType<GameSession>().HandlePlayerDeath();
    }

    private void FlipSprite()
    {
        if (IsPlayerMoving())
        {
            transform.localScale = new Vector2(Mathf.Sign(rb.velocity.x), 1f);
        }
    }

    public bool IsPlayerAlive()
    {
        return isAlive;
    }
}
