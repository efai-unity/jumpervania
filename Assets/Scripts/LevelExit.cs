using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelExit : MonoBehaviour
{
    [SerializeField] float levelLoadTime = 2f;
    [SerializeField] float levelExitTime = 0.2f;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!FindObjectOfType<Player>().IsPlayerAlive())
        {
            return;
        }

        StartCoroutine(LoadNextLevel());
    }

    private IEnumerator LoadNextLevel()
    {
        Time.timeScale = levelExitTime;

        yield return new WaitForSecondsRealtime(levelLoadTime);
        
        Time.timeScale = 1f;

        int currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(currentSceneIndex + 1);
    }
}
