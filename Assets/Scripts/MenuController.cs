using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    [SerializeField] GameObject startButton;
    [SerializeField] GameObject continueButton;

    private void Start()
    {
        if (PlayerPreferences.GetProgress() < 1)
        {
            startButton.SetActive(true);
            continueButton.SetActive(false);
        }
        else
        {
            startButton.SetActive(false);
            continueButton.SetActive(true);
        }
    }
}
