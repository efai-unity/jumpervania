using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPreferences: MonoBehaviour
{
    const string PROGRESS_KEY = "progress";
    const string SCORE_KEY = "score";

    public static int GetProgress()
    {
        return PlayerPrefs.GetInt(PROGRESS_KEY);
    }

    public static void SetProgress(int level, bool rewrite = false)
    {
        if (level > GetProgress() || rewrite)
        {
            PlayerPrefs.SetInt(PROGRESS_KEY, level);
        }
    }

    public static int GetScore()
    {
        return PlayerPrefs.GetInt(SCORE_KEY);
    }

    public static void SetScore(int score)
    {
        PlayerPrefs.SetInt(SCORE_KEY, score);
    }
}
