using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameSession : MonoBehaviour
{
    [SerializeField] Text livesText;
    [SerializeField] Text scoreText;

    private int lives = 5;
    private int score;

    private void Awake()
    {
        score = PlayerPreferences.GetScore();

        int numberOfGameSessions = FindObjectsOfType<GameSession>().Length;

        if (numberOfGameSessions > 1)
        {
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Start()
    {
        livesText.text = lives.ToString();
        scoreText.text = PlayerPreferences.GetScore().ToString();
    }

    public void HandlePlayerDeath()
    {
        if (lives > 1)
        {
            TakeLife();
        }
        else
        {
            ResetGameSession();
        }
    }

    private void TakeLife()
    {
        --lives;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

        livesText.text = lives.ToString();
    }

    public void AddScore(int amount)
    {
        score += amount;
        scoreText.text = score.ToString();

        PlayerPreferences.SetScore(score);
    }

    private void ResetGameSession()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(0);
        PlayerPreferences.SetScore(0);
        Destroy(gameObject);
    }
}
