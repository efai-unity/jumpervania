using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ProgressController : MonoBehaviour
{
    void Start()
    {
        UpdateProgress(SceneManager.GetActiveScene().buildIndex);
    }

    private void UpdateProgress(int currentLevel)
    {
        // ignore scenes which are not levels (levels are in build settings from 1 to penultimate)
        if (currentLevel < 1)
        {
            return;
        }
        else if (currentLevel > (SceneManager.sceneCountInBuildSettings - 1))
        {
            PlayerPreferences.SetProgress(0);
        }
        else
        {
            if (PlayerPreferences.GetProgress() < currentLevel)
            {
                PlayerPreferences.SetProgress(currentLevel);
            }
        }
    }
}
